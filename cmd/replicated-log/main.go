package main

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/api"
	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func main() {
	models.InitMsgMemory()

	r := gin.Default()

	api.Init(r)

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
