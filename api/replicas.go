package api

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func makeReplicas(c *gin.Context, message models.Message, writeParam int) error {
	jsonData, err := json.Marshal(message)
	if err != nil {
		return err
	}

	ackChan := make(chan bool)

	go func() {
		url := "http://localhost:8081/api/replicated-log/v1/message-memory"

		req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
		if err != nil {
			log.Println(err)
			ackChan <- true
			return
		}
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		log.Println("Master sent request to Secondary 1")
		resp, err := client.Do(req)
		log.Println("Master received response from Secondary 1")
		if err != nil {
			log.Println(err)
			ackChan <- true
			return
		}

		ackChan <- true

		defer resp.Body.Close()
	}()

	go func() {
		url := "http://localhost:8082/api/replicated-log/v1/message-memory"

		req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
		if err != nil {
			log.Println(err)
			ackChan <- true
			return
		}
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		log.Println("Master sent request to Secondary 2")
		resp, err := client.Do(req)
		log.Println("Master received response from Secondary 2")
		if err != nil {
			log.Println(err)
			ackChan <- true
			return
		}

		ackChan <- true

		defer resp.Body.Close()
	}()

	// logic for waiting response from necessary amount of replicas
	for i := 1; i < writeParam; i++ {
		<-ackChan
	}

	return nil
}
