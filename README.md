This is master server of "replicated log" system.
It runs on :8080 port

To store message, make POST request on localhost:8080/api/replicated-log/v1/message-memory
with JSON body in such format:

{
    "message": "test message 1"
}

To receive all stored messages in master server, make GET request on localhost:8080/api/replicated-log/v1/message-memory

ITERATION 2
To send write concern parameter, add ?w={x} to the URL, where {x} place values 1 - 3.
Ex.: localhost:8080/api/replicated-log/v1/message-memory?w=1
